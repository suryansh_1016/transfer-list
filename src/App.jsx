import { useState } from 'react'
import './App.css'

function App() {

  const [items, setItems] = useState([
    { name: 'JS', side: 'left', checked: false },
    { name: 'HTML', side: 'left', checked: false },
    { name: 'CSS', side: 'left', checked: false },
    { name: 'TS', side: 'left', checked: false },
    { name: 'React', side: 'right', checked: false },
    { name: 'Angular', side: 'right', checked: false },
    { name: 'Vue', side: 'right', checked: false },
    { name: 'Svelte', side: 'right', checked: false }
  ]);

  const handleRight = () => {
    setItems(prevItems =>
      prevItems.map(item =>
        item.side === 'left' && item.checked ? { ...item, side: 'right', checked: false } : item
      )
    );
  };
  
  const handleLeft = () => {
    setItems(prevItems =>
      prevItems.map(item =>
        item.side === 'right' && item.checked ? { ...item, side: 'left', checked: false } : item
      )
    );
  };
  const handleAllRight = () => {
    setItems(prevItems =>
      prevItems.map(item =>
        item.side === 'left' ? { ...item, side: 'right' } : item
      )
    );
  };

  const handleAllLeft = () => {
    setItems(prevItems =>
      prevItems.map(item =>
        item.side === 'right' ? { ...item, side: 'left' } : item
      )
    );
  };

  const handleCheckboxChange = (name, checked) => {
    setItems(prevItems =>
      prevItems.map(item =>
        item.name === name ? { ...item, checked } : item
      )
    );
  };

  return (
    <section>
      <div className="container">
        <div className="left">
          <ul>
            {items
              .filter(item => item.side === 'left')
              .map(({ name, checked }) => (
                <li key={name}>
                  <input
                    type="checkbox"
                    checked={checked}
                    onChange={e => handleCheckboxChange(name, e.target.checked)}
                  />
                  {name}
                </li>
              ))}
          </ul>
        </div>
        <div className="middle">
          <button onClick={handleAllRight}>{`>>`}</button>
          <button onClick={handleRight}>{'>'}</button>
          <button onClick={handleLeft}>{'<'}</button>
          <button onClick={handleAllLeft}>{'<<'}</button>
        </div>
        <div className="right"><ul>
          {items
            .filter(item => item.side === 'right')
            .map(({ name, checked }) => (
              <li key={name}>
                <input
                  type="checkbox"
                  checked={checked}
                  onChange={e => handleCheckboxChange(name, e.target.checked)}
                />
                {name}
              </li>
            ))}
        </ul></div>
      </div>
    </section>
  )
}

export default App
